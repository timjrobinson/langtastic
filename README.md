# Flagtastic
Welcome to Flagtastic!

This app is a flag selection trivia quiz game coded in Python and using Lambda, ApiGateway, DynamoDB and CloudFormation. It's best consumed inside an AWS Cloud9 IDE.

## Setup Steps
- cd ~/environment/langtastic
- aws cloudformation create-stack --stack-name lt-dev --template-body file://./scripts/create-tables.yml
- deploy langtastic from AWS Resources panel
- open ports 3000 and 8080 on your server
- in proxy.js change the BASE_URL to the URL of your new APIGateway's for cloud9-langtastic. You can find this under APIGateway -> cloud9-langtastic -> Stages (in left sidebar) -> Prod
- npm install
- npm start
- node proxy.js // Run in another terminal
- navigate to <machine-url>:8080
 

## Overview

### Structure

- data - This contains all trivia game related data. Currently it's just a list of countries and their 2 letter country code for the flag choices. This is stored in the 'lambdaflags' public bucket in the root directory as /countries.txt
- game - This contains all the Lambda functions written in Python that makes Flagtastic work. They connect to dynamodb and s3 and are called via APIGateway calls. 
- scripts - Contains a CloudFormation yaml file for creating the DynamoDB tables
- src - All the frontend code for Flagtastic. This app is written in React and most of the code comes from [Mitch Gavan's excellent react quiz tutorial](https://github.com/mitchgavan/react-multi-choice-quiz)
- proxy.js - A simple proxy server that sends requests to your APIGateway functions and returns the result. This is used because of CORS. CORS is a browser security feature that blocks you from accessing another websites url using client side javascript. Adding CORS overrides to APIGateway didn't work when I attempted to do it so I created this proxy server to re-route requests instead. 
- template.yml - The CloudFormation file to create the lambda functions with APIGateway links and correct permissions to access DynamoDB / S3. 