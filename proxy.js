// content of index.js
const http = require('http');
const request = require("request");
const path = require("path");
const port = 3000

const BASE_URL = "https://5qsq2w8kdc.execute-api.us-west-2.amazonaws.com/Prod";

const requestHandler = (req, res) => {
  let requestBody = "";
  req.on('data', (data) => {
      requestBody += data;
  });
  req.on('end', () => {
    console.log("Request body: ", requestBody);
    
    var headers = {};
    headers["Access-Control-Allow-Origin"] = "*";
    headers["Access-Control-Allow-Methods"] = "POST, GET, PUT, DELETE, OPTIONS";
    headers["Access-Control-Allow-Credentials"] = false;
    headers["Access-Control-Max-Age"] = '86400'; // 24 hours
    headers["Access-Control-Allow-Headers"] = "X-Requested-With, X-HTTP-Method-Override, Content-Type, User-Agent, Accept";
    res.writeHead(200, headers);
    
    if (!requestBody) return res.end("{}");
    request.post(BASE_URL + req.url, { json: JSON.parse(requestBody) }, function (err, response, body) {
      res.end(JSON.stringify(body));
    });
  });
};

const server = http.createServer(requestHandler);

server.listen(port, (err) => {
  if (err) {
    return console.log('something bad happened', err);
  }

  if (!BASE_URL) {
    return console.error("ERROR: BASE_URL is not set in proxy.js. Please set this before running the proxy.");
  }
  console.log(`server is listening on ${port}`);
})