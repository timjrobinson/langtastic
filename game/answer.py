# Lambda function called when the user answers a question
# Takes user-id and answer and returns if the user got it correct and what the correct answer was
# In the 'answers' table question_id is '{game_code}-{question_num}', because a table can only have 2 indexes

import json
import datetime
import string
import random
import boto3
from botocore.exceptions import ClientError

client = boto3.client('dynamodb')

def handler(event, context):
    if 'body' in event:
        event = json.loads(event['body'])
    if 'code' not in event:
        return create_error_response("code is a required parameter", 400)
    code = event['code']
    
    if 'question_num' not in event:
        return create_error_response("question_num is a required parameter", 400)
    question_num = event['question_num']
    
    if 'player_id' not in event:
        return create_error_response("player_id is a required parameter", 400)
    player_id = event['player_id']
    
    if 'answer_num' not in event:
        return create_error_response("answer_num is a required parameter", 400)
    answer_num = event['answer_num']
        
    question_data = client.get_item(
        TableName='questions', 
        Key={'game_code': {'S': code}, 'question_num': {'N': question_num}}
    )
    if 'Item' not in question_data:
        return {
            'statusCode': 404,
            'body': {
                'error': f"Could not find question {question_num} for game {code}"
            },
            'headers': {'Content-Type': 'application/json'}
        }
        
    try:
        save_answer = client.put_item(
            TableName='answers',
            ConditionExpression = 'attribute_not_exists(question_id) AND attribute_not_exists(player_id)',
            Item = {
                'question_id': { 'S': f"{code}-{question_num}" },
                'player_id': { 'S': player_id },
                'answer': { 'N': answer_num }
            }
        )
    except ClientError as e:
        if e.response['Error']['Code'] == 'ConditionalCheckFailedException':
            return create_error_response("You have already answered this question", 400)
        
    question = question_data['Item']
    correct_answer = question['correct_answer']['N'] == answer_num
    return {
        'statusCode': 200,
        'body': json.dumps({
            'correct': correct_answer
        }),
        'headers': {'Content-Type': 'application/json'}
    }
