
import random
import boto3

s3 = boto3.resource('s3')

def get_countries_array():
    obj = s3.Object("lambdaflags", "countries.txt")
    countries_file = obj.get()['Body'].read().decode('utf-8')
    countries = []
    for line in countries_file.split('\n'):
        country_split = line.split('|')
        countries.append({
            "name": country_split[0],
            "code": country_split[1]
        })
    return countries
    
def generate_question():
    countries = get_countries_array()
    correct_answer = random.randint(0, 3)
    random.shuffle(countries)
    picked_countries = countries[0:4]
    question = {
        'text': picked_countries[correct_answer]['code'].lower(),
        'answers': list(map(lambda c: c['name'], picked_countries)),
        'correct_answer': correct_answer
    }
    return question