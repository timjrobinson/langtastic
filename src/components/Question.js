import React from 'react';

function Question(props) {
  let imgSrc = "https://s3-us-west-2.amazonaws.com/lambdaflags/images/flags/" + props.content + ".png";
  return (
    <h2 className="question"><img src={imgSrc} /></h2>
  );

}

Question.propTypes = {
  content: React.PropTypes.string.isRequired
};

export default Question;
