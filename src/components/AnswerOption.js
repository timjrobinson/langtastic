import React from 'react';

function AnswerOption(props) {

  let className = "radioCustomButton"
  if (props.answer && props.correctAnswer !== props.answer) {
    className += " incorrect"
  }
  return (
    <li className="answerOption">
      <input
        type="radio"
        className={className}
        name="radioGroup"
        checked={props.answerNum === parseInt(props.answer, 10)}
        id={props.answerNum}
        value={props.answerNum}
        disabled={props.answer}
        onChange={props.onAnswerSelected}
      />
      <label className="radioCustomLabel" htmlFor={props.answerNum}>
        {props.answerContent}
      </label>
    </li>
  );

}

AnswerOption.propTypes = {
  correctAnswer: React.PropTypes.string.isRequired,
  answerContent: React.PropTypes.string.isRequired,
  answer: React.PropTypes.string.isRequired,
  onAnswerSelected: React.PropTypes.func.isRequired
};

export default AnswerOption;
