/* global fetch */
import React, { Component } from 'react';
import update from 'react-addons-update';
import Quiz from './components/Quiz';
import Result from './components/Result';
import logo from './svg/logo.svg';
import './App.css';

const PROXY_URL = "http://" + window.location.hostname + ":3000";
let quizQuestions = [];

class App extends Component {

  constructor(props) {
    super(props);

    this.state = {
      counter: 0,
      correctAnswers: 0,
      questionId: 1,
      question: '',
      answerOptions: [],
      answer: '',
      correctAnswer: null,
      score: '',
      result: false
    };

    this.handleAnswerSelected = this.handleAnswerSelected.bind(this);
    this.createGame((err, code) => {
      this.getQuestions(code, (err, questions) => {
        this.loadQuestions(questions);
      });
    });
  }
  
  createGame(callback) {
    fetch(PROXY_URL + "/create", {
      body: JSON.stringify({"player_id": "tim"}),
      headers: {
        'user-agent': 'Mozilla/4.0 MDN Example',
        'content-type': 'application/json'
      },
      method: "POST"
    }).then(response => {
      return response.json();
    }).then(function (responseJson) {
      console.log(responseJson);
      callback(null, responseJson.code);
    });
  }

  getQuestion(gameCode, questionNum, callback) {
    let requestBody = {
      "code": gameCode,
      "question_num": questionNum + ""
    }
    console.log("Request: ", requestBody);
    fetch(PROXY_URL + "/question", {
      body: JSON.stringify(requestBody),
      headers: {
        'user-agent': 'Mozilla/4.0 MDN Example',
        'content-type': 'application/json'
      },
      method: "POST"
    }).then(response => {
      return response.json();
    }).then(function (returnData) {
      callback(null, returnData)
    });
  }

  getQuestions(gameCode, callback) {
    let requestBody = {
      "code": gameCode,
    };
    console.log("Request: ", requestBody);
    fetch(PROXY_URL + "/questions", {
      body: JSON.stringify(requestBody),
      headers: {
        'user-agent': 'Mozilla/4.0 MDN Example',
        'content-type': 'application/json'
      },
      method: "POST"
    }).then(response => {
      return response.json();
    }).then(function (returnData) {
      callback(null, returnData);
    });
  }

  loadQuestions(questions) {
    quizQuestions = questions;
    this.setState({
      question: quizQuestions[0].text,
      answerOptions: quizQuestions[0].answers,
      correctAnswer: quizQuestions[0].correct_answer
    });
  }
  
  handleAnswerSelected(event) {
    this.setUserAnswer(event.currentTarget.value);

    if (this.state.questionId < quizQuestions.length) {
        setTimeout(() => this.setNextQuestion(), 300);
    } else {
        setTimeout(() => this.setResults(), 300);
    }
  }

  setUserAnswer(answer) {
    let correctAnswer = quizQuestions[this.state.counter].correct_answer;
    let correctAnswers = this.state.correctAnswers;
    if (answer === correctAnswer) {
      correctAnswers++;
    }
    this.setState({
      correctAnswers: correctAnswers,
      answer: answer
    });
  }

  setNextQuestion() {
    const counter = this.state.counter + 1;
    const questionId = this.state.questionId + 1;

    this.setState({
        counter: counter,
        questionId: questionId,
        question: quizQuestions[counter].text,
        answerOptions: quizQuestions[counter].answers,
        correctAnswer: quizQuestions[counter].correct_answer,
        answer: ''
    });
  }

  setResults(result) {
    this.setState({result: true})
  }

  renderQuiz() {
    return (
      <Quiz
        answer={this.state.answer}
        answerOptions={this.state.answerOptions}
        correctAnswer={this.state.correctAnswer}
        questionId={this.state.questionId}
        question={this.state.question}
        questionTotal={quizQuestions.length}
        onAnswerSelected={this.handleAnswerSelected}
      />
    );
  }

  renderResult() {
    return (
      <Result score={this.state.correctAnswers} />
    );
  }
  
  renderLoading() {
    return (
      <div>Loading...</div>   
    );
  }

  render() {
    
    return (
      <div className="App">
        <div className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h2>Langtastic</h2>
        </div>
        {this.state.question ? this.state.result ? this.renderResult() : this.renderQuiz() : this.renderLoading()}
      </div>
    );
  }

}

export default App;
